<?php

/*
 * Event Calendar (module) for Elxis CMS 2008.x and 2009.x+
 *
 * @version		1.0
 * @package		Event Calendar (module)
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Get globals
global $lang;

// Check if Event Calendar (component) is installed
if ( file_exists($mosConfig_absolute_path.'/components/com_eventcalendar/eventcalendar.class.php') ) {
	require_once( $mosConfig_absolute_path.'/components/com_eventcalendar/eventcalendar.class.php' );
} else {
	echo 'Event Calendar (component) not found!<br />';
	return;
}

// Includes
if ( file_exists($mosConfig_absolute_path.'/modules/mod_eventcalendar/language/'.$lang.'.php') ) {
	require_once( $mosConfig_absolute_path.'/modules/mod_eventcalendar/language/'.$lang.'.php' );
} else {
	require_once( $mosConfig_absolute_path.'/modules/mod_eventcalendar/language/english.php' );
}


// Check if module is already declared
if (!class_exists('clsEventCalendarMOD')) {
	class clsEventCalendarMOD {

		// Itialize variables
		public $ec_year = '';
		public $ec_month = '';
		public $ec_day = '';
		public $ec_date = '';
		
		public $ec_modParams = null;
		public $ec_comParams = null;
		public $ec_comItemid = '';
		
		public $ec_weeknrs = array();
		public $ec_calendar = array();
		public $ec_events = array();

		/****************************/
		/*  The class' constructor  */
		/****************************/ 
		public function __construct($params, $database) {
			global $mainframe, $my, $Itemid;
			
			// Load params from Event Calendar (module)
			$this->ec_modParams = $params;
			
			// Load params from Event Calendar (component)
			$database->setQuery("SELECT params FROM #__components WHERE link='option=com_eventcalendar'", '#__', 1, 0);
			$result = $database->loadResult();
			$this->ec_comParams = new mosParameters($result);
			
			// Get MENUITEM for links to Event Calendar (component)
			$this->ec_comItemid = $Itemid;
			if ($mainframe->getCfg('sef') != 2) {
				$access = !$mainframe->getCfg('shownoauth');
				$query = "SELECT id FROM #__menu WHERE link='index.php?option=com_eventcalendar' AND published='1'"
				."\n AND ((language IS NULL) OR (language LIKE '%".$lang."%'))"
				.($access ? "\n AND access IN (".$my->allowed.")" : "");
				$database->setQuery($query, '#__', 1, 0);
				$this->ec_comItemid = intval($database->loadResult());
				if (!$this->ec_comItemid) { $this->ec_comItemid = $Itemid; }
			}
			
			// Set variables
			$this->ec_year = (string)date("Y", time());
			$this->ec_month = (string)date("m", time());
			$this->ec_day = (string)date("d", time());
			$this->ec_date = strtotime($this->ec_year."-".$this->ec_month."-".$this->ec_day);
		}

		/******************************/
		/*  The class' main function  */
		/******************************/ 
		public function main() {
			// standard functionality
			$this->loadCalendar();
			$this->drawCalendar();
		}
		
		/***************************************/
		/*  Prepare to display calendar table  */
		/***************************************/ 
		protected function loadCalendar() {
			global $fmanager;
			
			$week_startingday = $this->ec_comParams->get('week_startingday' , 0);
			
			// Get timestamps for first and last day of month
			$firstDay_stamp = mktime( 0, 0, 0, $this->ec_month, 1, $this->ec_year );
			$lastDay_stamp = mktime( 0, 0, 0, $this->ec_month + 1, 1, $this->ec_year );
			$lastDay_stamp = strtotime("yesterday", $lastDay_stamp);

			// Get how many days will be empty in the view before current month starts
			$prevMonth_days = intval( strftime("%w", $firstDay_stamp) ) - $week_startingday;
			$prevMonth_days = ($prevMonth_days < 0)?$prevMonth_days + 7:$prevMonth_days;

			// Get how many days will be empty in the view after current month ends
			$nextMonth_days = intval( 6 - strftime("%w", $lastDay_stamp) + $week_startingday);
			$nextMonth_days = ($nextMonth_days == 7)?0:$nextMonth_days;
			
			// Loading weeknumbers
			$working_date = $firstDay_stamp;
			// Get the number of weeks in the view
			for($i = 0; $i < ( $prevMonth_days + strftime("%d", $lastDay_stamp) + $nextMonth_days ) / 7 ; $i++ ) {
				$this->ec_weeknrs[] = ($fmanager->iswin) ? strftime("%U", $working_date) : strftime("%V", $working_date);
				$working_date = strtotime("+ 1 week", $working_date);
			}

			// Create calender matrix of dates for days of month
			$working_date = strtotime( "- ".$prevMonth_days." days", $firstDay_stamp);
			// Fill previous month days
			for ($i = 0; $i < $prevMonth_days; $i++) { 
				$this->ec_calendar[] = $working_date;
				$working_date = strtotime( "+ 1 day", $working_date);
			}
			// Fill current month days
			for ($i = 0; $i < strftime("%d", $lastDay_stamp); $i++) {
				$this->ec_calendar[] = $working_date;
				$working_date = strtotime( "+ 1 day", $working_date);
			}
			// Fill next month days
			for ($i = 0; $i < $nextMonth_days; $i++) { 
				$this->ec_calendar[] = $working_date;
				$working_date = strtotime( "+ 1 day", $working_date);
			}
			
			// Calculate events
			$this->calcEvents();
		}
		
		/*************************/
		/*  Draw calendar table  */
		/*************************/ 
		protected function drawCalendar() {
			global $mainframe, $mosConfig_absolute_path, $lang;

			// Load core HTML library
			mosCommonHTML::loadOverlib();

			// Display month table
			$thisMonth = mktime( 0, 0, 0, $this->ec_month, 1, $this->ec_year );
			$lastMonth = strtotime( "1 month ago", $thisMonth );
			$nextMonth = strtotime( "+ 1 month", $thisMonth );?>
			
			<table>
				<!-- Calendar table -->
				<tr>
					<!-- Weeknum empty -->
					<?php if ($this->ec_modParams->get('show_weeknumber', true)) { ?>
						<td class="weeknum_empty" rowspan="2"></td>
					<?php } ?>
					<!-- Navigation -->
					<td class="navprev" colspan="2">
						<?php
							if (($this->ec_month - 1) <= 0) {
								$month = '12';
								$year = $this->ec_year - 1;
							} else {
								$month = $this->ec_month - 1;
								$month = (strlen($month) == 1) ? ("0".$month) : $month;
								$year = $this->ec_year;
							}
						?>
						<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$year."&month=".$month."&Itemid=".$this->ec_comItemid, EVCALBASE."/all/".$year."/".$month."/") ?>"> << </a>
					</td>
					<td class="nav" colspan="3">
						<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".date("Y", $thisMonth)."&month=".date("m", $thisMonth)."&Itemid=".$this->ec_comItemid, EVCALBASE."/all/".date("Y", $thisMonth)."/".date("m", $thisMonth)."/") ?>">
							<?php echo strftime( "%B", $thisMonth ) . " " . strftime("%Y", $thisMonth) ?>
						</a>
					</td>
					<td class="navnext" colspan="2">
						<?php
							if (($this->ec_month + 1) > 12) {
								$month = '01';
								$year = $this->ec_year + 1;
							} else {
								$month = $this->ec_month + 1;
								$month = (strlen($month) == 1) ? ("0".$month) : $month;
								$year = $this->ec_year;
							}
						?>
						<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$year."&month=".$month."&Itemid=".$this->ec_comItemid, EVCALBASE."/all/".$year."/".$month."/") ?>"> >> </a>
					</td>
				</tr>
				<tr>
					<!-- Week names -->
					<?php for($dcount = 0; $dcount < 7; $dcount++) {?> 
						<td class="weeknames"><?php echo strftime("%a", $this->ec_calendar[$dcount]) ?></td>
					<?php }?>
				</tr>
				<?php for ($wcount = 1; $wcount <= count($this->ec_weeknrs); $wcount++) { ?>
					<tr>
						<?php
						if ($this->ec_modParams->get('show_weeknumber', true)) { ?>
							<!-- Week num -->
							<td class="weeknum">
								<?php 
									if ($this->ec_modParams->get('week_number_links', true)) {
										?>
										<a href="<?php echo sefRelToAbs( "/index.php?option=com_eventcalendar&task=weekview&year=".$this->ec_year."&week=".$this->ec_weeknrs[$wcount-1]."&Itemid=".$this->ec_comItemid, EVCALBASE."/all/".$this->ec_year."/".$this->ec_weeknrs[$wcount-1].".html") ?>">
											<?php echo $this->ec_weeknrs[$wcount-1]; ?>
										</a>	
									<?php } else {
										echo $this->ec_weeknrs[$wcount-1];
									} ?>
							</td>
							<?php
						}
						for ($wdcount = 1; $wdcount <= 7; $wdcount++) {
							$working_day = $this->ec_calendar[(($wcount * 7) - 7) + ($wdcount - 1)];
							if (isset($working_day)) {
								if (date("m", $working_day) < date("m", $this->ec_date)) { ?>
									<!-- Last month -->
									<td class="othermonth">
									<span><?php echo strftime("%b", $working_day) ?></span>
								<?php } elseif (date("m", $working_day) > date("m", $this->ec_date)) { ?>
									<!-- Next month -->
									<td class="othermonth">
									<span><?php echo strftime("%b", $working_day) ?></span>
								<?php } else { ?>
									<?php
									$marked = false;
									foreach ($this->ec_events as $day_event) {
										// Calculate recursion
										if ($this->calcRecursion($working_day, $day_event)) {
											// Calculate exception days
											$exceptions = $this->calcExceptions( $day_event );
											if (array_search($working_day, $exceptions) === false) {
												$marked = true;
											}
										}
									}
									if ($marked) { ?>
										<td class="<?php echo (strftime("%d", $working_day) == strftime("%d", time()))?'cur_':''; ?>day_event">
										<span>
											<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".$this->ec_year."&month=".$this->ec_month."&day=".strftime("%d", $working_day)."&Itemid=".$this->ec_comItemid, EVCALBASE."/all/".$this->ec_year."/".$this->ec_month."/".strftime("%d", $working_day).".html") ?>">
												<?php 
													echo (date("Y-m-d", $working_day) == strftime("Y-m-d", time()))?'<b>':'';
													echo strftime("%d", $working_day);
													echo (date("Y-m-d", $working_day) == strftime("Y-m-d", time()))?'</b>':'';
												?>
											</a>
										</span>
									<?php } else { ?>
										<td class="<?php echo (strftime("%d", $working_day) == strftime("%d", time()))?'cur_':''; ?>day">
										<span>
											<?php 
												echo (date("Y-m-d", $working_day) == strftime("Y-m-d", time()))?'<b>':'';
												echo strftime("%d", $working_day);
												echo (date("Y-m-d", $working_day) == strftime("Y-m-d", time()))?'</b>':'';
											?>
										</span>
									<?php } ?>
							 		</td>
								<?php }
							}
						}?>
					</tr>
				<?php }
				
				// Forth-coming table
				if ($this->ec_modParams->get('view_forth') == '1') { ?>
					<tr><td class="spacer"></td></tr>
					<tr>
						<td class="subtitle" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
							<h5><?php echo CX_EVCALMOD_FORTH_EVENTS; ?>:</h5>
						</td>
					</tr><tr>
						<td class="forth_events" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
							<?php
							$forth_count = 0;
							$displist = array();
							$working_day = strtotime( "+1 day", mktime(0, 0, 0, date("m"), date("d"), date("Y")) );
							for ($i = 1; $i <= $this->ec_modParams->get('view_forth_days'); $i++) {
								foreach ($this->ec_events as $day_event) {
									if ($forth_count <= $this->ec_modParams->get('view_forth_num')) {
										// Calculate recursion
										if ($this->calcRecursion($working_day, $day_event)) {
											// Calculate exception days
											$exceptions = $this->calcExceptions( $day_event );
											if (array_search($working_day, $exceptions) === false) {
												// Prevent events been displayed more than once
												if (array_search($day_event->id, $displist) === false) {   ?>
													<li><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=".$day_event->id."&Itemid=".$this->ec_comItemid, EVCALBASE."/".$day_event->seotitle.".html") ?>"><?php echo $day_event->title; ?></a></li>
													<?php $forth_count++;
													$displist[] = $day_event->id;
												}
											}
										}
									}
								}
								$working_day = strtotime("+1 day", $working_day);
							} 
							if ($forth_count == 0) { ?>
								<p><?php echo CX_EVCALMOD_FORTH_NONE; ?></p>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>

				
				<?php // Day info table
				if ($this->ec_modParams->get('view_info') == '1') { 
					require_once( $mosConfig_absolute_path.'/modules/mod_eventcalendar/sun/sun.php' );
					require_once( $mosConfig_absolute_path.'/modules/mod_eventcalendar/sun/moon-phase.cls.php' );
					if ( file_exists($mosConfig_absolute_path.'/modules/mod_eventcalendar/anniversaries/'.$lang.'.php') ) {
						require_once( $mosConfig_absolute_path.'/modules/mod_eventcalendar/anniversaries/'.$lang.'.php' );
						$ann = 'true';
					}
					
					$objMoon = new moonPhase(-1);
					$moon = round( $objMoon->getPeriodInDays() - $objMoon->getDaysUntilNextNewMoon(), 0);
					$moon_name = $objMoon->getPhaseName();

					if ( $moon_name == "New Moon" or $moon == 0 ) {
						$moon_name = CX_EVCALMOD_INFO_NEWMOON;
					} elseif ( $moon_name == "First Quarter Moon" ) {
						$moon_name = CX_EVCALMOD_INFO_FQMOON;
					} elseif ( $moon_name == "Full Moon" ) {
						$moon_name = CX_EVCALMOD_INFO_FULLMOON;
					} elseif ( $moon_name == "Third Quarter Moon" ) {
						$moon_name = CX_EVCALMOD_INFO_TQMOON;
					} else {
						$moon_name = '';
					}
					
					if ( $moon_name ) {
						$moon_text = CX_EVCALMOD_INFO_MOONPRE . ":<br/>" . $moon_name;
					} else {
						if ( $moon == 1 ) {
							$moon_post = CX_EVCALMOD_INFO_MOONPOSTSING;
						} else {
							$moon_post = CX_EVCALMOD_INFO_MOONPOSTPLUR;
						}
						$moon_text = CX_EVCALMOD_INFO_MOONPRE . "<br />" . $moon . "&nbsp;" . $moon_post;
					} ?>
					
					<tr><td class="spacer"></td></tr>
					<tr>
						<td class="subtitle" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
							<h5><?php echo CX_EVCALMOD_INFO_TODAY.', '.$this->ec_year.'/'.$this->ec_month.'/'.$this->ec_day; ?></h5>
						</td>
					</tr>
					<?php if ($this->ec_modParams->get('view_info_place')) { ?>
						<tr>
							<td class="place" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
								<?php echo $this->ec_modParams->get('view_info_place'); ?>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<td class="sun-moon" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
							<table><tr>
								<td class="sun">
									<p>
										<?php echo CX_EVCALMOD_INFO_SUNRISE; ?>:&nbsp;<?php echo date("H:i", sun::rise($this->ec_date, $this->ec_modParams->get('view_info_timezone'), $this->ec_modParams->get('view_info_latitude'), $this->ec_modParams->get('view_info_longitude'), $this->ec_modParams->get('view_info_zenith') )); ?><br/>
										<?php echo CX_EVCALMOD_INFO_SUNSET; ?>:&nbsp;<?php echo date("H:i", sun::set($this->ec_date, $this->ec_modParams->get('view_info_timezone'), $this->ec_modParams->get('view_info_latitude'), $this->ec_modParams->get('view_info_longitude'), $this->ec_modParams->get('view_info_zenith') )); ?>
									</p>
								</td><td class="moon">
									<p><?php echo $moon_text; ?></p>
								</td>
							</tr></table>
						</td>
					</tr>
					
					<!-- Nameday table -->
					<?php if ($ann) { 
						calcSpecialNamedays($this->ec_day, $this->ec_month, $namedays_long, $namedays_short, $moving_holidays); ?>
						<tr><td class="spacer"></td></tr>
						<tr><td class="namedays" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
								<p><?php echo $namedays_long[$this->ec_day.'/'.$this->ec_month] ?><br/><?php echo isset($namedays_short[$this->ec_day.'/'.$this->ec_month]) ? '('.$namedays_short[$this->ec_day.'/'.$this->ec_month].')' : '' ?></p>
						</td></tr>
						
						<!-- Worldday table -->
						<tr><td class="spacer"></td></tr>
						<tr><td class="worlddays" colspan="<?php echo ($this->ec_modParams->get('show_weeknumber', true))?'8':'7'; ?>">
							<p><?php echo getWorldday($this->ec_day.'/'.$this->ec_month, $worlddays); ?></p>
						</td></tr>
					<?php }
				} ?>
			</table>
		<?php
		}

		/********************/
		/* Calculate events */
		/********************/
		protected function calcEvents() {
			global $database, $my;
			
			// Load events from database
			$query = "SELECT e.id, e.start_date, e.end_date FROM #__eventcalendar e"
			. "\n LEFT JOIN #__categories c ON c.id = e.catid"
			. "\n WHERE e.published = '1' AND c.published = '1' AND c.access IN (".$my->allowed.")"
			. "\n ORDER BY e.start_date ASC";
			$database->setQuery( $query );
			$results = $database->loadObjectList();

			// Calculate events
			foreach ($results as $row_event) {
				if ( (strtotime($row_event->start_date) <= $this->ec_calendar[count($this->ec_calendar)-1]) || (strtotime($row_event->end_date) >= $this->ec_calendar[0]) ) {
					$load_event = new mosEventCalendar_Event($database);
					$load_event->load($row_event->id);
					$this->ec_events[] = clone( $load_event );
				}
			}
		}

		/************************/
		/* Calculate exceptions */
		/************************/
		public function calcExceptions( $event ) {
			$events = split("\n", $event->recur_except);
			for ($i = 0; $i <= (count($events)-1); $i++) {
				$events[$i] = strtotime($events[$i]);
			}
			return $events;
		}
		
		/***********************/
		/* Calculate recursion */
		/***********************/
		public function calcRecursion ( $date, $event ) {
			
			// Check if date is in event publishing range
			if ( ((strtotime(date("Y-m-d", strtotime($event->start_date)))) <= $date) && ((strtotime(date("Y-m-d", strtotime($event->end_date)))) >= $date) ) {
			
				switch ($event->recur_type) {
					case 'week':
						return ( date("w", $date) == $event->recur_week );
						break;
					case 'month':
						return ( $event->recur_month == date("j", $date) );
						break;
					case 'year':
						return ( ($event->recur_year_d."/".$event->recur_year_m ) ==  date("j/n", $date) );
						break;
					case 'day':
						return true;
						break;
					default:
						return false;
						break;
				}
			}
		}
	
	} // end class
} // end if class_exists

// Start module
$objEventCalendarMOD = new clsEventCalendarMOD($params, $database);
$objEventCalendarMOD->main();
unset($objEventCalendarMOD);

?>
