<?php 

/*
 * Event Calendar (module) for Elxis CMS 2008.x and 2009.x+
 *
 * @version		1.0
 * @package		Event Calendar (module)
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Math functions
function sind($degrees) { return sin(deg2rad($degrees)); }
function cosd($degrees) { return cos(deg2rad($degrees)); }
function tand($degrees) { return tan(deg2rad($degrees)); }
function atand($x) { return rad2deg(atan($x)); }
function asind($x) { return rad2deg(asin($x)); }
function acosd($x) { return rad2deg(acos($x)); }
?>
