<?php 

/*
 * Event Calendar (module) for Elxis CMS 2008.x and 2009.x+
 *
 * @version		1.0
 * @package		Event Calendar (module)
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

define('_ISO','charset=UTF-8');

define ('CX_EVCALMOD_WEEKNUM', 'Display week-numbers');
define ('CX_EVCALMOD_WEEKNUMD', 'You can select whether the week-number in month-view is displayed in front of the rows or not.');
define ('CX_EVCALMOD_WEEKLINK', 'Display week-numbers as links');
define ('CX_EVCALMOD_WEEKLINKD', 'You can select whether the week-numbers in the monthly view are displayed as links to corresponding week-views or not.');
define ('CX_EVCALMOD_FORTH', 'Display forthcoming events');
define ('CX_EVCALMOD_FORTHD', 'Set wether forthcoming events should be displayed after calendar table.');
define ('CX_EVCALMOD_FORTH_DAYS', 'Range');
define ('CX_EVCALMOD_FORTH_DAYSD', 'Set the time range for the forthcoming events (in days).');
define ('CX_EVCALMOD_FORTH_NUM', 'Number');
define ('CX_EVCALMOD_FORTH_NUMD', 'Set the number of displayed forthcoming events.');
	define ('CX_EVCALMOD_FORTH_EVENTS', 'Forthcoming events');
	define ('CX_EVCALMOD_FORTH_NONE', 'There are no forthcoming events.');
define ('CX_EVCALMOD_INFO_SPACER_FORTH', 'Forthcoming events');
define ('CX_EVCALMOD_INFO_SPACER_SUN', 'Day information');
define ('CX_EVCALMOD_INFO_SPACER_HISTORY', 'Today history');
define ('CX_EVCALMOD_INFO', 'Display day info');
define ('CX_EVCALMOD_INFOD', 'Set wether day info (place, sunrise/sunset, moonfases, namedays and worlddays) should be displayed after calendar table.');
define ('CX_EVCALMOD_INFO_PLACE', 'Place');
define ('CX_EVCALMOD_INFO_PLACED', 'Set your place (leave empty to hide).');
define ('CX_EVCALMOD_INFO_TIMEZONE', 'Timezone');
define ('CX_EVCALMOD_INFO_TIMEZONED', 'Set your timezone.');
define ('CX_EVCALMOD_INFO_LAT', 'Latitude');
define ('CX_EVCALMOD_INFO_LATD', 'Set the decimal value for geographic latitude of your location.');
define ('CX_EVCALMOD_INFO_LONG', 'Longitude');
define ('CX_EVCALMOD_INFO_LONGD', 'Set the decimal value for geographic longitude of your location.');
define ('CX_EVCALMOD_INFO_ZENITH', 'Zenith');
define ('CX_EVCALMOD_INFO_ZENITHD', 'Set the zenith value of your location.');
	define ('CX_EVCALMOD_INFO_TODAY', 'Today');
	define ('CX_EVCALMOD_INFO_SUNRISE', 'Sunrise');
	define ('CX_EVCALMOD_INFO_SUNSET', 'Sunset');
	define ('CX_EVCALMOD_INFO_MOONPRE', 'Moon');
	define ('CX_EVCALMOD_INFO_MOONPOSTSING', 'day');
	define ('CX_EVCALMOD_INFO_MOONPOSTPLUR', 'days');
	define ('CX_EVCALMOD_INFO_NEWMOON', 'New Moon');
	define ('CX_EVCALMOD_INFO_FQMOON', 'First Quarter Moon');
	define ('CX_EVCALMOD_INFO_FULLMOON', 'Full Moon');
	define ('CX_EVCALMOD_INFO_TQMOON', 'Third Quarter Moon');
?>
