<?php 

/*
 * Event Calendar (module) for Elxis CMS 2008.x and 2009.x+
 *
 * @version		1.0
 * @package		Event Calendar (module)
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

define('_ISO','charset=UTF-8');

define ('CX_EVCALMOD_WEEKNUM', 'Εμφάνιση αριμθού εβδομάδων');
define ('CX_EVCALMOD_WEEKNUMD', 'Μπορείτε να ορίσετε εάν θα εμφανίζεται ο αριθμός της εβδομάδας στην αρχή κάθε σειράς.');
define ('CX_EVCALMOD_WEEKLINK', 'Εμφάνιση αριθμού εβδομάδων ως σύνδεσμο');
define ('CX_EVCALMOD_WEEKLINKD', 'Μπορείτε να ορίσετε εάν θα εμφανίζονται οι αριθμοί των εβδομάδων ως σύνδεσμοι προς τις αντίστοιχες εβδομαδιαίες προβολές.');
define ('CX_EVCALMOD_FORTH', 'Προβολή προσεχών εκδηλώσεων');
define ('CX_EVCALMOD_FORTHD', 'Ορίστε εάν θα εμφανίζονται οι προσεχείς εκδηλώσεις μετά το ημερολόγιο.');
define ('CX_EVCALMOD_FORTH_DAYS', 'Εύρος');
define ('CX_EVCALMOD_FORTH_DAYSD', 'Ορίστε το χρονικό εύρος των προσεχών εκδηλώσεων (σε ημέρες).');
define ('CX_EVCALMOD_FORTH_NUM', 'Πλήθος');
define ('CX_EVCALMOD_FORTH_DAYSD', 'Ορίστε το πλήθος των εμφανιζόμενων προσεχών εκδηλώσεων.');
	define ('CX_EVCALMOD_FORTH_EVENTS', 'Προσεχείς εκδηλώσεις');
	define ('CX_EVCALMOD_FORTH_NONE', 'Δεν υπάρχουν προσεχείς εκδηλώσεις.');
define ('CX_EVCALMOD_INFO_SPACER_FORTH', 'Προσεχείς εκδηλώσεις');
define ('CX_EVCALMOD_INFO_SPACER_SUN', 'Ταυτότητας ημέρας');
define ('CX_EVCALMOD_INFO_SPACER_HISTORY', 'Σαν σήμερα...');
define ('CX_EVCALMOD_INFO', 'Προβολή ταυτότητας ημέρας');
define ('CX_EVCALMOD_INFOD', 'Ορίστε εάν θα εμφανίζεται η ταυτότητα της ημέρας (τόπος, ανατολή/δύση ηλίου, φάση της σελήνης και ονομαστικές εορτές) μετά το ημερολογίο.');
define ('CX_EVCALMOD_INFO_PLACE', 'Τοποθεσία');
define ('CX_EVCALMOD_INFO_PLACED', 'Ορίστε την τοποθεσία σας (αφήστε κενό για απόκρυψη).');
define ('CX_EVCALMOD_INFO_TIMEZONE', 'Ζώνη ώρας');
define ('CX_EVCALMOD_INFO_TIMEZONED', 'Ορίστε την ζώνη ώρας της τοποθεσίας σας.');
define ('CX_EVCALMOD_INFO_LAT', 'Γεωγρ.Πλάτος');
define ('CX_EVCALMOD_INFO_LATD', 'Ορίστε την δεκαδική τιμή του γεωγραφικού πλάτους της τοποθεσίας σας.');
define ('CX_EVCALMOD_INFO_LONG', 'Γεωγρ.Μήκος');
define ('CX_EVCALMOD_INFO_LONGD', 'Ορίστε την δεκαδική τιμή του γεωγραφικού μήκους της τοποθεσίας σας..');
define ('CX_EVCALMOD_INFO_ZENITH', 'Κλίση ήλιου');
define ('CX_EVCALMOD_INFO_ZENITHD', 'Ορίστε την τιμή κλίσης του ήλιου (zenith) για την τοποθεσία σας.');
	define ('CX_EVCALMOD_INFO_TODAY', 'Σήμερα');
	define ('CX_EVCALMOD_INFO_SUNRISE', 'Ανατ.');
	define ('CX_EVCALMOD_INFO_SUNSET', 'Δύση');
	define ('CX_EVCALMOD_INFO_MOONPRE', 'Σελήνη');
	define ('CX_EVCALMOD_INFO_MOONPOSTSING', 'ημέρας');
	define ('CX_EVCALMOD_INFO_MOONPOSTPLUR', 'ημερών');
	define ('CX_EVCALMOD_INFO_NEWMOON', 'Νέα Σελήνη');
	define ('CX_EVCALMOD_INFO_FQMOON', 'Πρώτο Τέταρτο');
	define ('CX_EVCALMOD_INFO_FULLMOON', 'Πανσέληνος');
	define ('CX_EVCALMOD_INFO_TQMOON', 'Τελευταίο Τέταρτο');
?>
